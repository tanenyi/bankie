//
//  ActionsPage.swift
//  Bankie
//
//  Created by Enyi on 1/3/21.
//

import UIKit
import CoreData

/*
Implementation Assumptions
--------------------------
1. Client should not pay himself (but code allows for the edge case)
2. Client will only enter numerics
*/

class ActionsPage: UIViewController
{
	var client: Client!
	
	@IBOutlet weak var welcomeLabel: UILabel!
	@IBOutlet weak var balanceLabel: UILabel!
	
	@IBAction func topup(_ sender: Any) { promptAmountToAdd() }
	
	/// Prompt user to select from list of clients
	@IBAction func pay(_ sender: Any)
	{
		let alert = UIAlertController(title: "Pay",
									  message: "Select the user",
									  preferredStyle: .alert)
		Client.Database.forEach { client in
			// Ignore if database entry is ownself
			guard self.client.name != client.name else { return }
			alert.addAction(UIAlertAction(title: client.name, style: .default)
				{ _ in
				self.promptAmountToAdd(to: client)
				self.balanceLabel.text = self.client.printBalance
			})
		}
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
		present(alert, animated: true)
	}
	
	/// Prompt user to enter amount to topup
	/// - Parameter client: client to pay or self (topup) if nil
	func promptAmountToAdd(to client: Client? = nil)
	{
		let msg = client == nil ? "your account" : "\(client!.name)'s account"
		let alert = UIAlertController(title: "Topup",
									  message: "Enter amount to add to \(msg)",
									  preferredStyle: .alert)
		// Only allow user to enter numbers
		alert.addTextField { $0.keyboardType = .decimalPad }
		alert.addAction(UIAlertAction(title: "Ok", style: .default)
		{ _ in
			// Capture text entry as Int to topup
			if let entry = alert.textFields?.first?.text,
			   let amount = Double(entry)
			{
				// If client is others, pay them
				if let client = client { self.client.pay(client, amount: amount) }
				// Else topup self
				else {self.client.topup(amount) }
				self.balanceLabel.text = self.client.printBalance
			}
			else { fatalError("Not able to parse topup value") }
		})
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
		present(alert, animated: true)
	}
	
	override func viewDidLoad()
	{
        super.viewDidLoad()
		welcomeLabel.text = client.printGreeting
		balanceLabel.text = client.printBalance
    }
}
