//
//  Client.swift
//  Bankie
//
//  Created by Enyi on 1/3/21.
//

import Foundation

/*
Implementation Assumptions
--------------------------
1. Client can "pay" himself (essentially a topup)
2. Clients will not be removed from database
3. Persistent storage ignored since the focus is on transaction capability
*/

class Client
{
	typealias Element = Double
	
	/// Class to hold records of future payments
	class FuturePayments
	{
		// Payment to
		var payee: Client
		// Payment amount
		var amount: Element
		
		init(_ payee: Client, amount: Element)
		{
			self.payee = payee
			self.amount = amount
		}
		
		/// Alter client's balance after settling respective future payments
		/// - Parameter value: amount to settle
		func payOff(_ amount: Element)
		{
			self.amount -= amount
			payee.topup(amount)
		}
	}
	
	/// Create sample users for ease. Will eventually be superceded by database implementation
	static var Database: [Client] =
		{
			let alice = Client("Alice")
			let bob = Client("Bob")
			let charlie = Client("Charlie")
			return [alice, bob, charlie]
		}()
	
	var name: String
	var balance: Element { didSet { print("\(name): \(printBalance)") } }
	var futurePayments = [FuturePayments]()
	
	var printGreeting: String { "Hello, \(name)!\n\(printBalance)" }
	var printBalance: String { "Your balance is \(balance)." }
	
	init(_ name: String)
	{
		self.name = name
		self.balance = 0
	}
	
	/// Topup balance
	/// - Parameter amount: amount to topup
	func topup(_ amount: Element)
	{
		// Ignore if amount less than 1
		guard amount > 0 else { return }
		// If no future payment (outstanding payments)
		guard let payment = futurePayments.first
		else
		{
			balance += amount
			return
		}
		// Find minimum payable
		let available = min(payment.amount, amount)
		// Pay off debt with minimum payable
		payment.payOff(available)
		print("\(name): Transferred \(available) to \(payment.payee.name).")
		print(printBalance)
		// If debt fully paid, remove entry
		if payment.amount == 0 { futurePayments.removeFirst() }
		else { print("\(name): Owing \(payment.amount) to \(payment.payee.name).") }
		// Calculate remaining amount after payment
		let newAmount = amount - available
		// If still have balance, try to pay next future payment if have
		if newAmount > 0 { topup(newAmount) }
	}
	
	/// Pay client
	/// - Parameters:
	///   - payee: client to pay
	///   - amount: amount to pay
	func pay(_ payee: Client, amount: Element)
	{
		// Ignore if amount less than 1
		guard amount > 0 else { return }
		// If payee is ownself, topup to ownself
		guard payee.name != name else
		{
			topup(amount)
			return
		}
		// Find minimum payable
		let available = min(balance, amount)
		// Topup minimum payable to payee
		payee.topup(available)
		print("\(name): Transferred \(available) to \(payee.name).")
		// Reduce payer's balance
		balance -= available
		// Calculate remaining amount after transfer
		let newAmount = amount - available
		// If outstanding payment, create future payment and place in stack
		if newAmount > 0
		{
			let payment = FuturePayments(payee, amount: newAmount)
			futurePayments.append(payment)
			print("\(name): Owing \(newAmount) to \(payee.name).")
		}
	}
	
	/// Search for client in database
	/// - Parameter name: name of client
	/// - Returns: client object or nil if not found
	static func find(_ name: String) -> Client?
	{ Database.first { $0.name.lowercased() == name.lowercased() } }
	
	/// Add client to database
	/// - Parameter name: name of client
	/// - Returns: client object
	static func add(_ name: String) -> Client
	{
		let client = Client(name)
		Database.append(client)
		return client
	}
	
	/// Logs in client, creating client account if new
	/// - Parameter name: name of client
	/// - Returns: client object
	static func login(_ name: String) -> Client
	{
		let client = find(name) ?? add(name)
		print(client.printGreeting)
		return client
	}
}
