//
//  HomePage.swift
//  Bankie
//
//  Created by Enyi on 27/2/21.
//

import UIKit
import CoreData

/*
Implementation Assumptions
--------------------------
1. Password is not needed to login
2. Login will always use existing client or create new one if none found
*/

class HomePage: UIViewController
{
	@IBOutlet weak var username: UITextField!
	
	@IBAction func loginAction(_ sender: Any)
	{
		// Login or create user with name as text entry
		let client = Client.login(username.text!)
		// Segue to action page
		performSegue(withIdentifier: "login", sender: client)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		guard let client = sender as? Client
		else { fatalError("Segue not properly invoked") }
		guard let vc = segue.destination as? ActionsPage
		else { fatalError("Segue destination not correct") }
		vc.client = client
	}
}

