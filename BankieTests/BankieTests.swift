//
//  BankieTests.swift
//  BankieTests
//
//  Created by Enyi on 27/2/21.
//

import XCTest
@testable import Bankie

/*
Tests Assumptions
-----------------
1. Transaction tests need not cover cents since this is basic arithmetic
2. UI tests are not needed since the focus is not on the interface
*/

class BankieUnitTests: XCTestCase
{
	let aliceName = "Alice"
	let bobName = "Bob"
	let charlieName = "Charlie"
	let mollyName = "Molly"
	let balance = 75.0
	
	/// Remove the new client after each test has completed to start on a fresh slate
	override func tearDown()
	{
		Client.Database.removeAll { $0.name == mollyName }
		Client.Database.forEach {
			$0.balance = 0
			$0.futurePayments = []
		}
	}
	
	/// Test client add method
	func testAdd()
	{
		// Check client does not exists in database since not added yet
		Client.Database.forEach { XCTAssertNotEqual($0.name, mollyName) }
		// Add client (ignore returned object to manually search)
		let _ = Client.add(mollyName)
		// Manually find newly created client
		let newClient = Client.Database.first { $0.name == mollyName }
		// Newly created client must exist
		XCTAssertNotNil(newClient)
		// Newly created client must have same name as set
		XCTAssertEqual(newClient!.name, mollyName)
	}
	
	func testFind()
	{
		// Check client (not added) does not exist using client find method
		XCTAssertNil(Client.find(mollyName))
		// Manually add client
		Client.Database.append(Client(mollyName))
		// Check client exists using client find method
		XCTAssertNotNil(Client.find(mollyName))
	}
	
	func testLogin()
	{
		// Manually access database to get Alice
		let alice = Client.Database.first { $0.name == aliceName }!
		// Manually change Alice's balance
		alice.balance = balance
		// Store number of clients in database
		let numClientsBefore = Client.Database.count
		// Alice login (existing account)
		let clientA = Client.login(aliceName)
		// Store number of clients in database
		var numClientsAfter = Client.Database.count
		// There should be no change in numbers since Alice is existing client
		XCTAssertEqual(numClientsBefore, numClientsAfter)
		// Alice should reflect a new balance(manually set)
		XCTAssertEqual(clientA.balance, balance)
		
		// Molly login (new account)
		let _ = Client.login(mollyName)
		// Store number of clients in database
		numClientsAfter = Client.Database.count
		// There should be an increase of clients with new user
		XCTAssertEqual(numClientsAfter - numClientsBefore, 1)
		
		// Bob login different case (existing account)
		let _ = Client.login("bob")
		// Store number of clients in database
		numClientsAfter = Client.Database.count
		// There should not be an increase of clients with existing user
		XCTAssertEqual(numClientsAfter - numClientsBefore, 1)
		
		// Charlie login different case (existing account)
		let _ = Client.login("chArLie")
		// Store number of clients in database
		numClientsAfter = Client.Database.count
		// There should not be an increase of clients with existing user
		XCTAssertEqual(numClientsAfter - numClientsBefore, 1)
	}
	
	func testPay()
	{
		// Manually access database to get Alice
		let alice = Client.Database.first { $0.name == aliceName }!
		// Manually access database to get Bob
		let bob = Client.Database.first { $0.name == bobName }!
		// Manually change Alice's balance
		alice.balance = balance
		
		alice.pay(bob, amount: 100)
		// Alice:	75 - 75	= 0 (owe Bob 100 - 75 = 25)
		// Bob:		0 + 75	= 75
		XCTAssertEqual(alice.balance, 0)
		XCTAssertEqual(bob.balance, 75)
		XCTAssertEqual(alice.futurePayments.count, 1)
		XCTAssertEqual(alice.futurePayments.first!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.first!.amount, 25)
		
		bob.pay(alice, amount: 10)
		// Alice:	0 + 0			= 0 (owe Bob 25 - 10 = 15)
		// Bob:		75 - 10 + 10	= 75
		XCTAssertEqual(alice.balance, 0)
		XCTAssertEqual(bob.balance, 75)
		XCTAssertEqual(alice.futurePayments.count, 1)
		XCTAssertEqual(alice.futurePayments.first!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.first!.amount, 15)
		
		// Pay to self. Should have the same effect as topup
		alice.pay(alice, amount: 10)
		// Alice:	0 + 0	= 0 (owe Bob 15 - 10 = 5)
		// Bob:		75 + 10	= 85
		XCTAssertEqual(alice.balance, 0)
		XCTAssertEqual(bob.balance, 85)
		XCTAssertEqual(alice.futurePayments.count, 1)
		XCTAssertEqual(alice.futurePayments.first!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.first!.amount, 5)
		
		bob.pay(alice, amount: 10)
		// Alice:	0 + 5	= 5 (owe Bob 5 - 5 = 0) [remove]
		// Bob:		85 - 10 + 5	= 80
		XCTAssertEqual(alice.balance, 5)
		XCTAssertEqual(bob.balance, 80)
		XCTAssertEqual(alice.futurePayments.count, 0)
		
		// Ignore negative payments
		bob.pay(alice, amount: -100)
		XCTAssertEqual(alice.balance, 5)
		XCTAssertEqual(bob.balance, 80)
		
		// Ignore zero payments
		bob.pay(alice, amount: 0)
		XCTAssertEqual(alice.balance, 5)
		XCTAssertEqual(bob.balance, 80)
		
		alice.pay(bob, amount: 25)
		// Alice:	5 - 5	= 0 (owe Bob 25 - 5 = 20)
		// Bob:		80 + 5	= 85
		XCTAssertEqual(alice.balance, 0)
		XCTAssertEqual(bob.balance, 85)
		XCTAssertEqual(alice.futurePayments.count, 1)
		XCTAssertEqual(alice.futurePayments.first!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.first!.amount, 20)
		
		// Manually create more future payment records
		let charlie = Client(charlieName)
		alice.futurePayments.append(Client.FuturePayments(charlie, amount: 60))
		alice.futurePayments.append(Client.FuturePayments(bob, amount: 50))
		XCTAssertEqual(alice.futurePayments.count, 3)
		XCTAssertEqual(alice.futurePayments.first!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.first!.amount, 20)
		XCTAssertEqual(alice.futurePayments[1].payee.name, charlieName)
		XCTAssertEqual(alice.futurePayments[1].amount, 60)
		XCTAssertEqual(alice.futurePayments.last!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.last!.amount, 50)
		
		// Pay to self. Should have the same effect as topup
		alice.pay(alice, amount: 100)
		// Alice:	0 + 80	= 80 (owe Bob 20 - 20 = 0) [remove]
		// Bob:		85 + 20	= 105
		// vvvvvvvvvvvvvvvvvvvvvv
		// Alice:	0 + 0	= 0 (owe Charlie 60 - 60 = 0) [remove]
		// Bob:		85 + 20	= 105
		// Charlie: 0 + 60	= 60
		// vvvvvvvvvvvvvvvvvvvvvv
		// Alice:	0 + 0	= 0 (owe Bob 50 - 20 = 30)
		// Bob:		105 + 20	= 125
		// Charlie: 0 + 60	= 60
		XCTAssertEqual(alice.balance, 0)
		XCTAssertEqual(bob.balance, 125)
		XCTAssertEqual(charlie.balance, 60)
		XCTAssertEqual(alice.futurePayments.count, 1)
		XCTAssertEqual(alice.futurePayments.first!.payee.name, bobName)
		XCTAssertEqual(alice.futurePayments.first!.amount, 30)
	}
	
	func testTopup()
	{
		// Manually access database to get Alice
		let alice = Client.Database.first { $0.name == aliceName }!
		// Manually access database to get Bob
		let bob = Client.Database.first { $0.name == bobName }!
		// Manually change Alice's balance
		alice.balance = balance
		// Manually set Bob owes Alice 60
		bob.futurePayments = [Client.FuturePayments(alice, amount: 60)]
		
		bob.topup(40)
		// Alice:	75 + 40	= 115
		// Bob:		0 + 0	= 0  (owe Alice 60 - 40 = 20)
		XCTAssertEqual(alice.balance, 115)
		XCTAssertEqual(bob.balance, 0)
		XCTAssertEqual(bob.futurePayments.count, 1)
		XCTAssertEqual(bob.futurePayments.first!.payee.name, aliceName)
		XCTAssertEqual(bob.futurePayments.first!.amount, 20)
		
		bob.topup(40)
		// Alice:	115 + 20	= 135
		// Bob:		0 + 20		= 20  (owe Alice 20 - 20 = 0) [remove]
		XCTAssertEqual(alice.balance, 135)
		XCTAssertEqual(bob.balance, 20)
		XCTAssertEqual(bob.futurePayments.count, 0)
		
		// Ignore negative topups
		bob.topup(-40)
		XCTAssertEqual(alice.balance, 135)
		XCTAssertEqual(bob.balance, 20)
		
		// Ignore zero topups
		bob.topup(0)
		XCTAssertEqual(alice.balance, 135)
		XCTAssertEqual(bob.balance, 20)
	}
}

class BankieIntegrationTests: XCTestCase
{
	let aliceName = "Alice"
	let bobName = "Bob"
	
    func testSession()
	{
		let alice = Client.login(aliceName)
		alice.topup(100)
		let bob = Client.login(bobName)
		bob.topup(80)
		// Alice:	100
		// Bob:		80
		XCTAssertEqual(alice.balance, 100)
		XCTAssertEqual(bob.balance, 80)
		XCTAssertEqual(bob.futurePayments.count, 0)
		
		bob.pay(alice, amount: 50)
		// Alice:	100 + 50	= 150
		// Bob:		80 - 50		= 30
		XCTAssertEqual(alice.balance, 150)
		XCTAssertEqual(bob.balance, 30)
		XCTAssertEqual(bob.futurePayments.count, 0)
		
		bob.pay(alice, amount: 100)
		// Alice:	150 + 30	= 180
		// Bob:		30 - 30		= 0 (owe Alice 100 - 30 = 70)
		XCTAssertEqual(alice.balance, 180)
		XCTAssertEqual(bob.balance, 0)
		XCTAssertEqual(bob.futurePayments.count, 1)
		XCTAssertEqual(bob.futurePayments.first!.payee.name, aliceName)
		XCTAssertEqual(bob.futurePayments.first!.amount, 70)
		
		bob.topup(30)
		// Alice:	180 + 30	= 210
		// Bob:		0 - 0		= 0 (owe Alice 70 - 30 = 40)
		XCTAssertEqual(alice.balance, 210)
		XCTAssertEqual(bob.balance, 0)
		XCTAssertEqual(bob.futurePayments.count, 1)
		XCTAssertEqual(bob.futurePayments.first!.payee.name, aliceName)
		XCTAssertEqual(bob.futurePayments.first!.amount, 40)
		
		alice.pay(bob, amount: 30)
		// Alice:	210 - 30 + 30	= 210
		// Bob:		0 + 0			= 0 (owe Alice 40 - 30 = 10)
		XCTAssertEqual(alice.balance, 210)
		XCTAssertEqual(bob.balance, 0)
		XCTAssertEqual(alice.futurePayments.count, 0)
		XCTAssertEqual(bob.futurePayments.count, 1)
		XCTAssertEqual(bob.futurePayments.first!.payee.name, aliceName)
		XCTAssertEqual(bob.futurePayments.first!.amount, 10)
		
		bob.topup(100)
		// Alice:	210 + 10	= 220
		// Bob:		0 + 90		= 90 (owe Alice 10 - 10 = 0)
		XCTAssertEqual(alice.balance, 220)
		XCTAssertEqual(bob.balance, 90)
		XCTAssertEqual(bob.futurePayments.count, 0)
    }
}
